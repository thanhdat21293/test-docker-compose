FROM node:10.10.0

# Set the work directory
RUN mkdir -p /data/app

WORKDIR /data/app

# Use Cache Please
ADD . /data/app

# Kiểm tra lỗi npm i
# RUN npm audit
# RUN npm --verbose install
RUN npm install && npm cache clean --force
# RUN npm audit
# RUN npm install -g typescript
# RUN npm install -g ts-node
RUN npm run build
# Add application files

EXPOSE 3000

CMD ["node", "index.js"]
